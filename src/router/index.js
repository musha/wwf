import Vue from "vue";
import VueRouter from "vue-router";
import index from "@/views/index.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "index",
        component: index
    },
    // {
    //     path: "/",
    //     name: "home",
    //     component: home
    // },
    // {
    //     path: "/poster",
    //     name: "poster",
    //     component: () =>
    //         import("../views/poster.vue")
    // }
];

const router = new VueRouter({
    routes
});

export default router;

// console.log('index page');
import MtaH5 from 'mta-h5-analysis';
export default {
	data() {
		return {
			page: 'index'
		};
	},
	mounted() {
		var context = this;
        this.Request = new Object();
        this.Request = this.GetRequest();//拿到当前路径参数
        this.$root.click_id = this.Request.gdt_vid
        // console.log(JSON.stringify(this.Request))
        $.ajax({
            type:'POST',
            url:'https://wwf.uglobal.com.cn/sendaction.php',
            dataType:"json",
            data:{
                'url': 'https://wwf.uglobal.com.cn/refusingivory',
                'action_type': 'RESERVATION',
                'click_id': this.$root.click_id
            },
            complete: ( res ) =>{
                // console.log(res)
            }
        })
        MtaH5.pgv();
        gtag('config', 'UA-151682819-5', {'page_title': 'wwfemo2_pv1'}); 
		//
	},
	methods: {
		GetRequest() {
            var url = location.href; 
         
            url = decodeURIComponent(decodeURIComponent(url))
            // console.log(url)
            var theRequest = new Object();
            //有参数
            if (url.indexOf("?") != -1) {
                var str = url.split("?")
                str.forEach(item =>{
                    //至少两个参数
                    if(item.indexOf('&') != -1){
                        var  strs = item.split("&");
                        for(var i = 0; i < strs.length; i ++) {
                            theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
                        }
                    } else {
                        //只有一个参数
                        if(item.indexOf('=') != -1){
                            var strs = item.split("=");
                            theRequest[strs[0]] = decodeURIComponent(strs[1].replace("/","").replace("#",""));
                        }
                    }
                })
            }
            return theRequest;
       }
	}
};
